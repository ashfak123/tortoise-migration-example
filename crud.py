from models import Contact


async def create_contact(name, phone, email):
    contact = await Contact.create(name=name, phone=phone, email=email)
    return contact

async def list_contact():
    return await Contact.all()