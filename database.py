from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise


TORTOISE_ORM = {
    "connections": {"default": "postgres://postgres:root@localhost:5432/tutorial1"},
    "apps": {
        "contact": {
            "models": [
                "models", "aerich.models"
            ],
            "default_connection": "default",
        },
    },
}


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url="postgres://postgres:root@localhost:5432/tutorial1",
        modules={"models": [
            "models"
        ]},
        generate_schemas=False,
        add_exception_handlers=True,
    )
