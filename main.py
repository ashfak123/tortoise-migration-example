from typing import List
from fastapi import FastAPI
from pydantic import BaseModel
from database import init_db
from crud import create_contact, list_contact

app = FastAPI()

class Contact(BaseModel):
    name: str
    phone: str
    email: str


@app.on_event("startup")
async def startup_event():
    init_db(app)


@app.post('/contact')
async def add_contact(contact: Contact):
    new_contact = await create_contact(**contact.dict())
    return new_contact.id


@app.get('/contact', response_model=List[Contact])
async def all_contacts():
    contacts = await list_contact()
    return contacts
