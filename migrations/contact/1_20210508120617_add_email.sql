-- upgrade --
ALTER TABLE "contact" ADD "email" VARCHAR(100) NOT NULL;
-- downgrade --
ALTER TABLE "contact" DROP COLUMN "email";
