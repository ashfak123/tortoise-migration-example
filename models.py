from tortoise.models import Model
from tortoise import fields

class Contact(Model):
    name = fields.CharField(max_length=255)
    phone = fields.CharField(max_length=20)
    email = fields.CharField(max_length=100)

    def __str__(self):
        return self.name